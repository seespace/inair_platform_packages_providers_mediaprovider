/* //device/content/providers/media/src/com/android/providers/media/MediaScannerReceiver.java
**
** Copyright 2007, The Android Open Source Project
**
** Licensed under the Apache License, Version 2.0 (the "License"); 
** you may not use this file except in compliance with the License. 
** You may obtain a copy of the License at 
**
**     http://www.apache.org/licenses/LICENSE-2.0 
**
** Unless required by applicable law or agreed to in writing, software 
** distributed under the License is distributed on an "AS IS" BASIS, 
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
** See the License for the specific language governing permissions and 
** limitations under the License.
*/

package com.android.providers.media;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.os.UserHandle;
import android.os.SystemProperties;
import java.io.File;

import java.io.File;
import java.io.IOException;

public class MediaScannerReceiver extends BroadcastReceiver {
    private final static String TAG = "MediaScannerReceiver";

    //scan policy,from 0 ~ 32
    private final static int NO_SCAN = 0;
    private final static int STANDARD_SCAN = 1;//scan the standard device defined in android only, means external_storage
    //you can add other scan policy here
    private final static int ALL_SCAN = 32;//scan all the device,max value

    //extend flag
    private final static int SYSSCAN_FLAG = 64;//used as a bit flag

    private static int getScanPolicy(){
        int policy = 0;
        String scanPolicy = SystemProperties.get("sys.mediascanner.enable", "true");
        if( scanPolicy.equals("false") )
            policy = NO_SCAN;
        else if( scanPolicy.equals("standard") )//only scan the external device 
            policy = STANDARD_SCAN | SYSSCAN_FLAG;
        else if( scanPolicy.equals("true") )
            policy =  ALL_SCAN | SYSSCAN_FLAG;
        else if( scanPolicy.equals("cts") )//only scan the external device 
            policy =  STANDARD_SCAN;
        return policy;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        int policy = getScanPolicy();
        boolean bSysScan = (policy & SYSSCAN_FLAG) > 0;
        policy = policy & (SYSSCAN_FLAG - 1);//unset the syscan flag
        Log.d(TAG,"onReceive policy "+policy +" systemscan "+bSysScan);
    
        final String action = intent.getAction();
        final Uri uri = intent.getData();
        if (Intent.ACTION_BOOT_COMPLETED.equals(action)) {
            // Scan both internal and external storage
            scan(context, MediaProvider.INTERNAL_VOLUME, null);

            if( bSysScan ){
                if ( policy >= STANDARD_SCAN ){
                    //scan the emulated storage
                    Log.d(TAG,"scan emulated externalStorage "+Environment.getExternalStorageDirectory().getPath());
                    scan(context, MediaProvider.EXTERNAL_VOLUME, Environment.getExternalStorageDirectory().getPath());
                }

                if ( policy == ALL_SCAN ) {
                    //if not owner, we will not receive the mount message
                    //so we need to scan the all the external storage by self
                    if(UserHandle.myUserId() != 0){
                        String state = Environment.getExternalStorage2State();
                        if (Environment.MEDIA_MOUNTED.equals(state) ||
                                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
                            Log.v(TAG, "to scan the mounted externalstorage2 "+Environment.getExternalStorage2Directory().getPath());
                            scan(context, MediaProvider.EXTERNAL_VOLUME, Environment.getExternalStorage2Directory().getPath());                
                        }
                    } else {
                        Log.d(TAG,"owner don't need to scan the check externalstorage2");
                    }
                }
            }
            
        } else {
            if (uri.getScheme().equals("file")) {
                // handle intents related to external storage
                String path = uri.getPath();
                String externalStoragePath = Environment.getExternalStorageDirectory().getPath();
                String legacyPath = Environment.getLegacyExternalStorageDirectory().getPath();

                try {
                    path = new File(path).getCanonicalPath();
                } catch (IOException e) {
                    Log.e(TAG, "couldn't canonicalize " + path);
                    return;
                }
                if (path.startsWith(legacyPath)) {
                    path = externalStoragePath + path.substring(legacyPath.length());
                }

                Log.d(TAG, "action: " + action + " path: " + path);
                if (Intent.ACTION_MEDIA_MOUNTED.equals(action)) {
                    boolean system_send = intent.getBooleanExtra("system-send", false);
                    if (system_send) {
                        if( isDeviceNeedScan(policy,path) && bSysScan) 
                            scan(context, MediaProvider.EXTERNAL_VOLUME, path);
                        else
                            Log.d(TAG,path+" system scan skipped");
                    } else {
                        if(policy == STANDARD_SCAN)//only scan the external storage
                            scan(context, MediaProvider.EXTERNAL_VOLUME, externalStoragePath);
                        else if(policy == ALL_SCAN) {
                            //for app doesn't know there are internal flash exists,
                            //so when it force to scan, we force scan all devices.
                            scanAll(context, MediaProvider.EXTERNAL_VOLUME);
                        }
                    }
                } else if (Intent.ACTION_MEDIA_SCANNER_SCAN_FILE.equals(action) &&
                        path != null && path.startsWith(externalStoragePath + "/")) {
                    scanFile(context, path);
                }
            }
        }
    }

    private boolean isDeviceNeedScan(int policy, String devPath){
        if( new File(devPath).equals( Environment.getExternalStorageDirectory()) ){//external storage
            if( policy >= STANDARD_SCAN )
                return true;
        } else if( policy == ALL_SCAN ) {
                return true;
        }

        return false;
    }

    private void scan(Context context, String volume, String path) {
        Bundle args = new Bundle();
        args.putString("volume", volume);
        args.putString("path", path);
        context.startService(
                new Intent(context, MediaScannerService.class).putExtras(args));
    }

    private void scanAll(Context context, String volume) {
        Bundle args = new Bundle();
        args.putString("volume", volume);
        args.putBoolean("all", true);
        context.startService(
                new Intent(context, MediaScannerService.class).putExtras(args));
    }

    private void scanFile(Context context, String path) {
        Bundle args = new Bundle();
        args.putString("filepath", path);
        context.startService(
                new Intent(context, MediaScannerService.class).putExtras(args));
    }    
}
